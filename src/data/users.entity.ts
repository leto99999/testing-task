import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
  JoinColumn
} from 'typeorm'
import { LeadsEntity } from './leads.entity'

@Entity()
export class UsersEntity {
  @PrimaryGeneratedColumn()
  public id: number

  @Column()
  public user_id_amo: number

  @Column({ nullable: true })
  public name: string
 

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: Date

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt!: Date

}
