import { Injectable, Logger, OnModuleInit } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { In, Not, Repository, Like, ILike } from 'typeorm'
import { LeadsEntity } from './leads.entity'
import { ContactsEntity } from './contacts.entity'
import { UsersEntity } from './users.entity'
import { AppService } from '../app.service'
import axios from 'axios'
import { Cron, CronExpression } from '@nestjs/schedule'
import { IContact, IUser, ICustomFields } from '../interfaces/interfaces'
import { UtilsService } from 'src/utils/utils.service'

// TODO: wrap into try catch

@Injectable()
export class DataService {
  constructor(
    private appService: AppService,
    @InjectRepository(LeadsEntity)
    private leadsRepository: Repository<LeadsEntity>,
    @InjectRepository(ContactsEntity)
    private contactsRepository: Repository<ContactsEntity>,

    @InjectRepository(UsersEntity)
    private usersRepository: Repository<UsersEntity>,

    private utilsService: UtilsService,
  ) {}

  private readonly logger = new Logger(DataService.name)

  /* ----------------------------------- CRON START ----------------------------------- */
  // Check for changes & save to DB
  // + covert responsible user's ids ––> responsible user's names

  @Cron(CronExpression.EVERY_10_SECONDS)
  async syncLeadsAndContacts(): Promise<void> {
    this.logger.debug('Called every 10 seconds')

    const latestUpdatedTimeFromDB = await this.getLatestUpdatedTimeFromDB()
    const numberLatestUpdatedTimeFromDB = latestUpdatedTimeFromDB * 1000

    const leadsFromAmo = await this.getLeadsWithContactsId()
    /* ----------------------------------- zz ----------------------------------- */
    //NOTE - 111

    // console.log(leadsFromAmo)
    let isChanges = false

    for (let leadFromAmo of leadsFromAmo) {
      leadFromAmo.updated_at = new Date(leadFromAmo.updated_at * 1000)
      const timeDifference = leadFromAmo.updated_at - numberLatestUpdatedTimeFromDB

      if (timeDifference > 0) {
        isChanges = true
        const contactsIdsArrayFromAmoLead: object[] = await this.getIdsArrayFromAmoEntity(
          leadFromAmo._embedded.contacts,
        )
        console.log(contactsIdsArrayFromAmoLead)

        const leadFromDB = await this.getleadFromDB(leadFromAmo)

        let leadFromAmoUnixTimeStemp = (leadFromAmo.updated_at / 1000).toString()

        // interface A {
        //   lead_id_amo: number
        //   name: string
        //   price: number | string
        //   responsible_user_id_amo: string
        //   status_id_amo: string
        //   contacts_ids_amo: string
        //   tags_amo: string
        //   lead_updated_at_amo: string
        // }

        const freshLeadFromAMO = {
          lead_id_amo: leadFromAmo.id,
          name: leadFromAmo.name,
          price: leadFromAmo.price,
          responsible_user_id_amo: leadFromAmo.responsible_user_id,
          status_id_amo: leadFromAmo.status_id,
          contacts_ids_amo: JSON.stringify(contactsIdsArrayFromAmoLead),
          tags_amo: JSON.stringify(leadFromAmo._embedded.tags),
          lead_updated_at_amo: leadFromAmoUnixTimeStemp,
        }

        const saved = await this.leadsRepository.save(Object.assign(leadFromDB || {}, freshLeadFromAMO))

        await this.setManyToManyLeadsToContactsRelations(saved, contactsIdsArrayFromAmoLead)
      }
    }

    // covert user's ids ––> user's names ()
    if (isChanges) {
      const leads: any[] = await this.getLeadsWithContactsId()
      const users: any[] = await this.getUsers()
      const usersNamesAndIdsObj = this.utilsService.putUsersNamesAndIdInObj(users)
      const leadsWithUserNames = this.utilsService.changeNameIdForNameText(leads, usersNamesAndIdsObj)
      await this.saveLeadsToBD(leadsWithUserNames)
      isChanges = false
    }
  }
  /* ----------------------------------- CRON END ----------------------------------- */

  async getLatestUpdatedTimeFromDB(): Promise<number> {
    const query = this.leadsRepository.createQueryBuilder('leads_entity')
    query.select('MAX(leads_entity.lead_updated_at_amo)', 'max')
    const result = await query.getRawOne()
    return result.max
  }

  // async getLeadsWithContactsId(): Promise<any[]> {
  async getLeadsWithContactsId(): Promise<any[]> {
    const tokensFromDB = await this.appService.getTokensFromDB()
    try {
      let answer = await axios({
        method: 'get',
        url: 'https://nickolaisisin1.amocrm.ru/api/v4/leads?with=contacts',
        headers: {
          Authorization: `Bearer ${tokensFromDB.access_token}`,
          'Content-Type': 'application/json',
        },
      })
      // console.log(answer.data._embedded.leads);

      return answer.data._embedded.leads
    } catch (error) {
      console.log('Something went wrong...', error)
    }
  }

  async getUsers(): Promise<any[]> {
    const tokensFromDB = await this.appService.getTokensFromDB()

    try {
      const answer = await axios({
        method: 'get',
        url: 'https://nickolaisisin1.amocrm.ru/api/v4/users',
        headers: {
          Authorization: `Bearer ${tokensFromDB.access_token}`,
          'Content-Type': 'application/json',
        },
      })
      return answer.data._embedded.users
    } catch (error) {
      console.log('Something went wrong...', error)
    }
  }

  async getContacts(): Promise<object[]> {
    const tokensFromDB = await this.appService.getTokensFromDB()

    try {
      const answer = await axios({
        method: 'get',
        url: 'https://nickolaisisin1.amocrm.ru/api/v4/contacts',
        headers: {
          Authorization: `Bearer ${tokensFromDB.access_token}`,
          'Content-Type': 'application/json',
        },
      })

      return answer.data._embedded.contacts
    } catch (error) {
      console.log('Something went wrong...', error)
    }
  }

  async saveContactsToBD(contacts: any[]) {
    // console.log(contacts);

    const savedIds: number[] = []

    for (const contact of contacts) {
      const customFields: ICustomFields[] = []

      const contactFromDB = await this.contactsRepository.findOne({
        where: {
          contact_id_amo: contact.id,
        },
      })

      for (const fields_values of contact.custom_fields_values) {
        if (fields_values.field_code === 'EMAIL') {
          customFields.push({ email: fields_values.values[0].value })
        }
        if (fields_values.field_code === 'PHONE') {
          customFields.push({ phone: fields_values.values[0].value })
        }
      }

      const freshContactFromAMO: IContact = {
        contact_id_amo: contact.id,
        name: contact.name,
        responsible_user_id_amo: contact.responsible_user_id,
        custom_fields: JSON.stringify(customFields),
        contact_updated_at_amo: contact.updated_at,
      }

      const saved = await this.contactsRepository.save(Object.assign(contactFromDB || {}, freshContactFromAMO))
      savedIds.push(saved.id)
    }

    await this.contactsRepository.delete({
      id: Not(In(savedIds)),
    })
  }

  async saveUsersToBD(users: any[]) {
    const savedIds: number[] = []

    for (const user of users) {
      const userFromDB = await this.usersRepository.findOne({
        where: {
          user_id_amo: user.id,
        },
      })

      const freshUserFromAMO: IUser = {
        user_id_amo: user.id,
        name: user.name,
      }

      const saved = await this.usersRepository.save(Object.assign(userFromDB || {}, freshUserFromAMO))
      savedIds.push(saved.id)
    }

    await this.usersRepository.delete({
      id: Not(In(savedIds)),
    })
  }

  async saveLeadsToBD(leads: any[]): Promise<void> {
    const savedIds: number[] = []

    for (const lead of leads) {
      const leadFromDB = await this.getleadFromDB(lead)

      const contactsIdsArrayFromAmoLead = await this.getIdsArrayFromAmoEntity(lead._embedded.contacts)

      //  renew leadFromDB из freshLeadFromAMO
      const freshLeadFromAMO = {
        lead_id_amo: lead.id,
        name: lead.name,
        price: lead.price,
        responsible_user_id_amo: JSON.stringify(lead.responsible_user_id),
        status_id_amo: lead.status_id,
        contacts_ids_amo: JSON.stringify(contactsIdsArrayFromAmoLead),
        tags_amo: JSON.stringify(lead._embedded.tags),
        lead_updated_at_amo: lead.updated_at,
      }

      // put freshLeadFromAMO into emty obj – that is, create new leads with changes and save it to DB
      const saved = await this.leadsRepository.save(Object.assign(leadFromDB || {}, freshLeadFromAMO))

      // set Many-To-Many relation ( Lead-To-Contacts )
      if (contactsIdsArrayFromAmoLead[0] !== undefined) {
        await this.setManyToManyLeadsToContactsRelations(saved, contactsIdsArrayFromAmoLead)
      }

      savedIds.push(saved.id)
    }

    await this.leadsRepository.delete({
      id: Not(In(savedIds)),
    })
  }

  async getIdsArrayFromAmoEntity(contactsAmoFieldObj: any): Promise<object[]> {
    let idsFromAmoEntity = []
    for (const elem of contactsAmoFieldObj) {
      idsFromAmoEntity.push({ id: elem.id })
    }
    return idsFromAmoEntity
  }
  async getleadFromDB(lead: { id: number }): Promise<LeadsEntity> {
    return await this.leadsRepository.findOne({
      where: {
        lead_id_amo: lead.id,
      },
    })
  }

  async setManyToManyLeadsToContactsRelations(lead: LeadsEntity, contactsIdsArray) {
    // console.log(contactsIdsArray);

    const contactsArryaForRelationsWithLeads = []

    for (let elem of contactsIdsArray) {
      const contact = await this.contactsRepository.findOne({
        where: {
          contact_id_amo: elem.id,
        },
      })
      contactsArryaForRelationsWithLeads.push(contact)
    }
    lead.contactsEntityIMNAP = contactsArryaForRelationsWithLeads // ( [], [], [] )
    await this.leadsRepository.save(lead)
  }

  /* ----------------------------------- here it needs to take leads from DB, if have "querystring" then give short list of leads ----------------------------------- */
  async getLeadsWithContactsRelationsDB(leadName?: string): Promise<object[]> {
    if (leadName) {
      const serchedLeadsWithContactsRelations = await this.leadsRepository.find({
        where: {
          name: ILike(`%${leadName}%`),
        },
        relations: ['contactsEntityIMNAP'],
      })
      return serchedLeadsWithContactsRelations
    }
    const leadsWithContactsRelations = await this.leadsRepository.find({
      relations: ['contactsEntityIMNAP'],
    })
    return leadsWithContactsRelations
  }

  async onModuleInit(): Promise<object[]> {
    this.logger.debug('onModuleInit resolved')
    await this.appService.checkTokens()
    const contacts: any[] = await this.getContacts()
    const leads: any[] = await this.getLeadsWithContactsId()
    const users: any[] = await this.getUsers()

    const usersNamesAndIdsObj = this.utilsService.putUsersNamesAndIdInObj(users)
    const leadsWithUserNames = this.utilsService.changeNameIdForNameText(leads, usersNamesAndIdsObj)

    await this.saveContactsToBD(contacts)
    await this.saveLeadsToBD(leadsWithUserNames)
    await this.saveUsersToBD(users)

    const leadsWithContactsRelationsDB = await this.getLeadsWithContactsRelationsDB()

    return leadsWithContactsRelationsDB
  }
}
