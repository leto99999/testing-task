import {Module} from '@nestjs/common';
import {DataService} from './data.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ContactsEntity} from "./contacts.entity";
import {LeadsEntity} from "./leads.entity";
import {Tokens} from "../app.entity";
import {AppService} from "../app.service";
import { UtilsModule } from 'src/utils/utils.module';
import { UsersEntity } from './users.entity';

@Module({
    imports: [TypeOrmModule.forFeature([LeadsEntity, ContactsEntity, Tokens, UsersEntity]), UtilsModule],
    providers: [DataService, AppService, ],
    exports: [DataService]
})
export class DataModule {
}
