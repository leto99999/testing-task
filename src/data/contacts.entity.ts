import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    ManyToMany,
} from 'typeorm';
import {LeadsEntity} from "./leads.entity";

@Entity()
export class ContactsEntity {
    @PrimaryGeneratedColumn()
    public id: number

    @Column()
    public contact_id_amo: number;

    @Column({nullable: true})
    public name: string;

    @Column({nullable: true})
    public responsible_user_id_amo: number;

    @Column({nullable: true})
    public contact_updated_at_amo: number;
    
    @Column({ nullable: true })
    public custom_fields: string

    @CreateDateColumn({type: 'timestamp'})
    public createdAt!: Date;

    @UpdateDateColumn({type: 'timestamp'})
    public updatedAt!: Date;

    @ManyToMany(() => LeadsEntity, (leadsEntity) => leadsEntity.contactsEntityIMNAP)

    leadsEntityIMNAP: LeadsEntity[]
}
