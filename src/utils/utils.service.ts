import { Injectable } from '@nestjs/common'

@Injectable()
export class UtilsService {
  constructor() {}

  putUsersNamesAndIdInObj(users: any[]): object {
    const usersNamesAndIdInObj = {}

    for (const elem of users) {
      const key = elem.id
      const value = elem.name
      usersNamesAndIdInObj[key] = value
    }
    return usersNamesAndIdInObj
  }

  changeNameIdForNameText(leads: any[], names: object): any[] {
    for (const elem of leads) {
      for (const item in names) {
        if (elem.responsible_user_id === +item) {
          elem.responsible_user_id = names[item]
        }
      }
    }
    return leads
  }
}
