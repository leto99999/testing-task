import { Controller, Get,  Query, Res } from '@nestjs/common'
import { AppService } from './app.service'
import { DataService } from './data/data.service'
import { Response } from 'express'

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly dataService: DataService,
  ) {}



  @Get()
  public async mainRoute(@Res() response: Response, @Query() query): Promise<object> {
    await this.appService.checkTokens()
    const answer = await this.dataService.getLeadsWithContactsRelationsDB(query.queryString) 
    return response.status(200).json({ answer })
  }
}
