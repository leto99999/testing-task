export interface IContact {
    contact_id_amo: number
    name: string
    responsible_user_id_amo: number
    custom_fields: string
    contact_updated_at_amo: number
}
export interface IUser {
    user_id_amo: number
    name: string
    
}

export interface IContactsAmoFieldObj {
    id: number
}

export interface ICustomFields {
    email?: string,
    phone?: string
}
    


